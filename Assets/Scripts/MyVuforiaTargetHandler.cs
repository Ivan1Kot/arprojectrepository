﻿using UnityEngine;

public class MyVuforiaTargetHandler : DefaultTrackableEventHandler
{
    [SerializeField]private AudioSource _source;

    protected override void OnTrackingFound()
    {
        _source.Play();
    }

    protected override void OnTrackingLost()
    {
        _source.Pause();
    }
}