﻿using UnityEngine;
using UnityEngine.EventSystems;

public delegate void MouseMoved(float xMovement, float yMovement);
public class InputManager : MonoBehaviour
{
    #region Events

    public static event MouseMoved MouseMoved;

    #endregion

    #region Private Fields

    private float _xMovement;
    private float _yMovement;

    #endregion

    #region Methods

    #region Unity Methods

    private void Update()
    {
        InvokeActionOnInput();
    }

    #endregion

    #region Private Methods

    private void InvokeActionOnInput()
    {
            _xMovement = Input.GetAxis("Mouse X");
            _yMovement = Input.GetAxis("Mouse Y");

            OnMouseMoved(_xMovement, _yMovement);
    }

    #endregion

    #region Event Methods

    private static void OnMouseMoved(float xmovement, float ymovement)
    {
        var handler = MouseMoved;

        if (handler != null)
        {
            handler(xmovement, ymovement);
        }
    }
   
    #endregion

    #endregion
}