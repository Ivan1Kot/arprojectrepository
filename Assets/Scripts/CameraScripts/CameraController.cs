﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class CameraController : MonoBehaviour
{
    #region Serialized Fields

    [SerializeField, Range(0.0f, 1.0f)] private float _lerpRate;
    [SerializeField, Range(0.0f, 1.0f)] private float _rotateSpeedModifier = 1f;

    #endregion

    #region Private Fields

    private float _xRotation;
    private float _yYRotation;
    private Transform _cachedTransform;
    private bool _canRotate;

    #endregion

    #region Methods

    #region Unity Methods

    private void Awake()
    {
        _cachedTransform = transform;
        InputManager.MouseMoved += Rotate;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _canRotate = !IsPointerOverUIObject();
        }
        _xRotation = Mathf.Lerp(_xRotation, 0, _lerpRate);
        _yYRotation = Mathf.Lerp(_yYRotation, 0, _lerpRate);
        _cachedTransform.eulerAngles += Vector3.up * -_xRotation * _rotateSpeedModifier;
        _cachedTransform.eulerAngles = (Vector3.up * _cachedTransform.eulerAngles.y) + (Vector3.forward * _cachedTransform.eulerAngles.z);

    }

    private void OnDestroy()
    {
        InputManager.MouseMoved -= Rotate;
    }

    #endregion

    #region Private Methods

    private void Rotate(float xMovement, float yMovement)
    {
        if (Input.GetMouseButton(0) && _canRotate)
        {
            _xRotation += Mathf.Clamp(xMovement, -5f, 5f);
            _yYRotation += Mathf.Clamp(yMovement, -5f, 5f);
        }
    }

    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    #endregion

    #endregion
}