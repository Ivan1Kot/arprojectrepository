﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelsManager : MonoBehaviour
{
	#region Public Methods

	public void GoToCameraLevel()
	{
		SceneManager.LoadScene("Main");
	}

	public void ExitApplication()
	{
		Application.Quit();
	}

	#endregion
}