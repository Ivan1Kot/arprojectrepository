﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
	#region Serialized Fields

	[SerializeField] private Transform _cam;

	#endregion

	#region Private Methods

	private void Update()
	{
		transform.LookAt(_cam);
	}

	#endregion
}